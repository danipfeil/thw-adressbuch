/* eslint-disable camelcase */
export interface dienststelle {
  street: string;
  code: string;
  oeId: number;
  zip: number;
  city: string;
  region: string;
  phone: string;
  fax: string;
  email: string;
  takzeich: string;
  name: string;
  coordinates?: (number)[] | null;
  leader?: (LeaderEntity)[] | null;
  link: string;
  type: dienststelleType;
  parents?: (string | null)[] | null;
  units?: (UnitsEntity)[] | null;
}

export enum dienststelleType {
  OV = 'OV',
  LV = 'LV',
  RST = 'RST',
  LT = 'LT',
  AZ = 'AZ'
}

export interface LeaderEntity {
  name: string;
  picture: string;
  title: string;
}
export interface UnitsEntity {
  name: string;
  link: string;
  teileinheit?: (TeileinheitEntity | null)[] | null;
}
export interface TeileinheitEntity {
  name: string;
  link: string;
  extra: string;
}

export interface ExtendedDienststelle extends dienststelle {
  distance: number
}

export interface FilterIndex {
  einheit: string[];
  teileinheit: string[];
}

export interface Commit {
  id: string;
  short_id: string;
  created_at: string;
  parent_ids?: (string)[] | null;
  title: string;
  message: string;
  author_name: string;
  author_email: string;
  authored_date: string;
  committer_name: string;
  committer_email: string;
  committed_date: string;
  web_url: string;
}
