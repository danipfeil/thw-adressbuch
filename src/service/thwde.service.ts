
import { ExtendedDienststelle, dienststelle, dienststelleType, FilterIndex, Commit } from 'src/components/models'
import distance from '@turf/distance'
class ThwdeService {
  thwdedata: dienststelle[] = [];
  datastoragekey = 'dienststellenjson'
  dataversionkey = 'dienststellenupdate'

  async InitData (): Promise<boolean> {
    const newVersion = await this.CheckIfNewVersionAvailable()
    if (newVersion || localStorage.getItem(this.datastoragekey) === null) {
      try {
        const resp = await fetch('https://cors.bridged.cc/https://gitlab.com/Manuel_Raven/dienststellen-scraper-extended/-/raw/master/dienststellen.json', {
          method: 'GET',
          headers: {
          },
          credentials: 'same-origin'
        })
        this.thwdedata = (JSON.parse(await resp.text() || '[]') as dienststelle[])
        localStorage.setItem(this.datastoragekey, JSON.stringify(this.thwdedata))
        return true
      } catch (error) {
        return false
      }
    } else {
      this.thwdedata = (JSON.parse(localStorage.getItem(this.datastoragekey) || '[]') as dienststelle[])
      return true
    }
  }

  async CheckIfNewVersionAvailable (): Promise<boolean> {
    const latestVersion: Commit | null = (JSON.parse(localStorage.getItem(this.dataversionkey) || 'null') as Commit | null)
    try {
      // eslint-disable-next-line camelcase
      const resp = await (await fetch(`https://gitlab.com/api/v4/projects/25364116/repository/commits?since=${latestVersion?.committed_date || ''}`, {
        method: 'GET',
        headers: {
        },
        credentials: 'same-origin'
      })).text()
      const newCommits: Commit[] = (JSON.parse(resp) as Commit[])
      // eslint-disable-next-line camelcase
      if (newCommits[0].short_id !== latestVersion?.short_id) {
        localStorage.setItem(this.dataversionkey, JSON.stringify(newCommits[0]))
        return true
      } else {
        return false
      }
    } catch (error) {
      return false
    }
    // eslint-disable-next-line camelcase
  }

  GetAllOV (latlong?: number[], searchName?: string, searchEinheit?: string, searchTeileinheit?: string, parent?: string): ExtendedDienststelle[] {
    let ovlist: ExtendedDienststelle[] = []
    ovlist = this.thwdedata.filter(x => x.type === dienststelleType.OV).map(x => {
      return {
        ...x, ...{ distance: (latlong && x.coordinates) ? this.GetDistance(latlong, x.coordinates) : 0 }
      }
    })
    if (parent && parent !== '*' && parent !== '') ovlist = ovlist.filter(x => x.parents?.includes(parent))
    if (searchName && searchName !== '*' && searchName !== '') ovlist = ovlist.filter(x => x.name.toLowerCase().includes(searchName.toLowerCase()))
    if (searchEinheit && searchEinheit !== '*' && searchEinheit !== '') {
      ovlist = ovlist.filter(x => {
        for (const einheit of x.units || []) {
          if (einheit.name === searchEinheit) return true
        }
        return false
      })
    }
    if (searchTeileinheit && searchTeileinheit !== '*' && searchTeileinheit !== '') {
      ovlist = ovlist.filter(x => {
        for (const einheit of x.units || []) {
          for (const teile of einheit.teileinheit || []) {
            if (teile?.name === searchTeileinheit) return true
          }
        }
        return false
      })
    }
    return ovlist
  }

  GetAllRst (latlong?: number[], parent?: string): ExtendedDienststelle[] {
    let rstlist = this.thwdedata.filter(x => x.type === dienststelleType.RST).map(x => {
      return {
        ...x, ...{ distance: (latlong && x.coordinates) ? this.GetDistance(latlong, x.coordinates) : 0 }
      }
    })
    if (parent && parent !== '*' && parent !== '') rstlist = rstlist.filter(x => x.parents?.includes(parent))
    return rstlist
  }

  GetAllLV (latlong?: number[]): ExtendedDienststelle[] {
    return this.thwdedata.filter(x => x.type === dienststelleType.LV).map(x => {
      return {
        ...x, ...{ distance: (latlong && x.coordinates) ? this.GetDistance(latlong, x.coordinates) : 0 }
      }
    })
  }

  GetLT () {
    return this.thwdedata.find(x => x.type === dienststelleType.LT)
  }

  GetAll (): dienststelle[] {
    return this.thwdedata
  }

  GetJSONIndex (): FilterIndex {
    const einheitlist: string[] = []
    const teileinheitlist: string[] = []

    const allov = this.GetAllOV()
    for (const ov of allov) {
      for (const einheit of ov.units || []) {
        if (!einheitlist.includes(einheit.name)) einheitlist.push(einheit.name)
        for (const teileinheit of einheit.teileinheit || []) {
          if (teileinheit && !teileinheitlist.includes(teileinheit.name)) teileinheitlist.push(teileinheit.name)
        }
      }
    }

    return { einheit: einheitlist, teileinheit: teileinheitlist }
  }

  GetDistance (latlong: number[], current: number[]): number {
    return parseFloat(distance(latlong, current, { units: 'kilometers' }).toFixed(2))
  }
}
export default new ThwdeService()
